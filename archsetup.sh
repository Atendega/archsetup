#!/usr/bin/env bash

reflector -c US -l 50 -f 10 --threads 4 --save /etc/pacman.d/mirrorlist
sgdisk --zap-all /dev/sda
sgdisk --new 1:0:+8M --typecode 1:ef00 --change-name 1:efi
sgdisk --largest-new 2 --typecode 2:8300 --change-name 2:archlinux
mkfs.fat -n efi /dev/sda1
mkfs.btrfs -f -L archlinux --csum xxhash /dev/sda2
mount /dev/sda2 -o relatime,compress=zstd:6 /mnt
cd /mnt; btrfs su cr @; btrfs su cr @home; btrfs su cr @log; btrfs su cr @pkg; btrfs su cr @snap
cd; umount /mnt; mount /dev/sda2 -o relatime,compress=zstd:6,subvol=@ /mnt
mkdir -p /mnt/{boot/efi,btrfs,home,var/log,var/cache/pacman/pkg}
mount /dev/sda1 -o noatime /mnt/boot/efi
mount /dev/sda2 -o noatime,compress=zstd:6,subvolid=5 /mnt/btrfs
mount /dev/sda2 -o relatime,compress=zstd:6,subvol=@home /mnt/home
mount /dev/sda2 -o noatime,compress=zstd:6,subvol=@log /mnt/var/log
mount /dev/sda2 -o noatime,compress=zstd:6,subvol=@pkg /mnt/var/cache/pacman/pkg
pacstrap -i /mnt base base-devel linux-zen linux-zen-headers nvidia-dkms intel-ucode grub efibootmgr networkmanager fish neovim bat exa fd ripgrep chezmoi git snapper snap-pac
reflector -c US -l 50 -f 10 --threads 4 --save /mnt/etc/pacman.d/mirrorlist
arch-chroot /mnt snapper -c root create-config root
btrfs su de /mnt/.snapshots
mkdir /mnt/.snapshots
mount /dev/sda2 -o relatime,compress=zstd:6,subvol=@snap /mnt/.snapshots
genfstab -t PARTLABEL /mnt >> etc/fstab
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /mnt/etc/locale.gen; arch-chroot /mnt locale-gen
systemd-firstboot --prompt
passwd
systemctl preset-all
systemctl enable NetworkManager snapper-boot.timer
arch-chroot /mnt grub-install
arch-chroot /mnt grub-mkconfig -o boot/grub/grub.cfg
arch-chroot /mnt nvim boot/grub/grub.config
sleep 1
echo 'Done!'
